import dotenv from 'dotenv';
import { AmbilightSide, Authentication, Colour, PhilipsTV } from './philipstv';

dotenv.config();

const username = process.env.TV_USER;
const password = process.env.TV_PASS;
const host = process.env.TV_HOST;

if (!host) {
    console.error('TV_HOST environment variable must be set.');
    process.exit(1);
}
if (!username || !password) {
    console.error(
        'USERNAME and PASSWORD environment variables must be set. Use the pair command to create credentials.'
    );
    process.exit(2);
}

const auth: Authentication = {
    user: username,
    pass: password,
    sendImmediately: false,
};
const philipsTv = new PhilipsTV(host, undefined, auth);

const COLOUR_VARIATIONS = 3;
// const colours: Colour[] = [
//     { r: 255, g: 0, b: 0 },
//     { r: 0, g: 255, b: 0 },
//     { r: 0, g: 0, b: 255 },
// ];
const colours = Array(COLOUR_VARIATIONS * 3)
    .fill(0)
    .map((_, index) => {
        const isRed = index < COLOUR_VARIATIONS;
        const isGreen = index >= COLOUR_VARIATIONS && index < 2 * COLOUR_VARIATIONS;
        const isBlue = index > 2 * COLOUR_VARIATIONS;
        const variation = index % COLOUR_VARIATIONS;
        const colourValue = (1 / COLOUR_VARIATIONS) * (variation + 1) * 255;

        return {
            r: isRed ? colourValue : 0,
            g: isGreen ? colourValue : 0,
            b: isBlue ? colourValue : 0,
        };
    });
console.log(colours);

function delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

async function loop() {
    const { layers, left, top, right, bottom } = await philipsTv.getAmbilightTopology();

    // Reset to white.
    philipsTv.setAmbilightColours({ r: 255, g: 255, b: 255 }).catch((res) => {
        console.log(res);
    });

    await delay(1000);

    for (let colourIndex = 0; colourIndex < colours.length; colourIndex++) {
        const colour = colours[colourIndex];
        for (let layer = 1; layer <= layers; layer++) {
            await loopSide(colour, layer, left, 'left');
            await loopSide(colour, layer, top, 'top');
            await loopSide(colour, layer, right, 'right');
            await loopSide(colour, layer, bottom, 'bottom');
        }
    }

    await delay(1000);

    for (let colourIndex = colours.length - 1; colourIndex >= 0; colourIndex--) {
        const colour = colours[colourIndex];
        for (let layer = layers; layer > 0; layer--) {
            await loopSide(colour, layer, bottom, 'bottom', true);
            await loopSide(colour, layer, right, 'right', true);
            await loopSide(colour, layer, top, 'top', true);
            await loopSide(colour, layer, left, 'left', true);
        }
    }

    await delay(1000);

    const BLACK = { r: 0, g: 0, b: 0 };
    const WHITE = { r: 255, g: 255, b: 255 };
    await philipsTv.setAmbilightColours(BLACK);
    await delay(100);
    await philipsTv.setAmbilightColours(WHITE);
    await delay(50);
    await philipsTv.setAmbilightColours(BLACK);
    await delay(100);
    await philipsTv.setAmbilightColours(WHITE);
    await delay(50);
    await philipsTv.setAmbilightColours(BLACK);
}

async function loopSide(colour: Colour, layer: number, count: number, side: AmbilightSide, reverse = false) {
    for (let light = 0; light < count; light++) {
        const actualLight = !reverse ? light : count - light;

        await philipsTv.setAmbilightColours({
            [`layer${layer}`]: {
                [side]: {
                    [actualLight]: colour,
                },
            },
        });
    }
}

loop().then(() => {
    console.log('all done');
});
