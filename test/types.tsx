// noinspection JSUnusedLocalSymbols
/* eslint-disable @typescript-eslint/no-unused-vars */

/**
 * @fileoverview Solely to test types. If TS throws an error. The type is not valid.
 */

import { AmbilightSetter } from '../src/philipstv';

const allLights: AmbilightSetter = {
    r: 0,
    g: 0,
    b: 0,
};
const allLightsOnLayer: AmbilightSetter = {
    layer1: {
        r: 0,
        g: 0,
        b: 0,
    },
};
const allLightsOnSide: AmbilightSetter = {
    layer1: {
        top: {
            r: 0,
            g: 0,
            b: 0,
        },
    },
};
const singleLight: AmbilightSetter = {
    layer1: {
        top: {
            0: { r: 0, g: 0, b: 0 },
            1: { r: 0, g: 0, b: 0 },
        },
        left: {
            0: { r: 0, g: 0, b: 0 },
        },
    },
};
